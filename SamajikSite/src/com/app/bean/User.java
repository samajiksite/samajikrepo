package com.app.bean;

public class User {
	
	String firstname,lastname,email,profilepiclink,number;

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getProfilepiclink() {
		return profilepiclink;
	}

	public void setProfilepiclink(String profilepiclink) {
		this.profilepiclink = profilepiclink;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@Override
	public String toString() {
		return "User [firstname=" + firstname + ", lastname=" + lastname + ", email=" + email + ", profilepiclink="
				+ profilepiclink + ", number=" + number + "]";
	}
	
	

}
